import scrapy
from googlesearch import search
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
import os
import asyncio
from pyppeteer import launch
import nest_asyncio

# query = "James D. Thomas"
queries = [
    'Dr. Zhaoyang Pan','Dr. Tien Chun Chen','Dr. Michael Borookhim',
    'Dr. Cheryl Tam Chu','Dr. Raymond M. Menchaca','Dr. James D. Thomas',
    'Dr. Ragaie L. Hakim','Dr. Nagy F. Khalil',
    'Dr. Hung Dinh Doan',
]


class DemoSpider(scrapy.Spider):
    name = "demo"

    def start_requests(self):
        for query in queries:
            check_dir(query)
            urls = []
            for url in search(query, num=10, stop=10, pause=5, country="countryUS"):
                urls.append(str(url))
            for url in urls:
                try:
                    yield scrapy.Request(url=url, callback=self.parse, cb_kwargs={'query': query})
                except:
                    print("Unable to load: " + url)

            for url in urls:
                try:
                    get_ss(url, query)
                except:
                    print("Unable to save screen shot")

    def parse(self, response, query):
        filename = "HTML\\" + query + "\\" + response.url.split("//")[1].split("/")[0] + '.html'
        with open(filename, 'wb') as f:
            f.write(response.body)
        self.log(f'Saved file {filename}')


def get_ss(url_, query_):
    async def main_ss():
        img_path = "HTML/" + query_ + "/" + url_.split("//")[1].split("/")[0] + ".png"
        browser = await launch()
        page = await browser.newPage()
        await page.goto(url_)
        await page.screenshot({'path': img_path, 'fullPage': 'true'})
        await browser.close()

    asyncio.get_event_loop().run_until_complete(main_ss())


def check_dir(query_):
    try:
        os.stat("HTML\\" + query_)
    except:
        os.mkdir("HTML\\" + query_)


nest_asyncio.apply()
process = CrawlerProcess(get_project_settings())
process.crawl(DemoSpider)
process.start()
