#!/usr/bin/env python
# coding: utf-8

# In[161]:


import pandas as pd
from fuzzywuzzy import fuzz
import re
import html2text
import nltk
from nltk.tokenize import word_tokenize
# nltk.download('punkt')
from fuzzysearch import find_near_matches


# In[48]:


# Doctor details
doc_name = "Dr. Ragaie L. Hakim"
npi = "1104952084"
address = "1557 E. Florence Ave Los Angeles, CA 90001"
ph_no = "323-277-0272"


# In[224]:


# Cleaning HTML

# file =  open("htmls/doctor.webmd.com.html", "r").read()
file =  open("htmls/www.topnpi.com.html", "r").read()

h = html2text.HTML2Text()

h.ignore_links = True
h.skip_internal_links = True
h.ignore_images = True

read_file = h.handle(file)


# ### Searching for similar string in large files

# In[69]:


for attr in [npi, address, ph_no,doc_name]:
    len_attr = int(min(max(1, len(attr)//2), 10))
    occur = find_near_matches(attr, read_file, max_l_dist=len_attr)
    if len(occur)>=1:
        print(str(attr) + " is present in the file")
        print(occur[:4])
#         for ocr in occur:
#             val = ocr.matched
#             score = fuzz.token_sort_ratio(val, attr)
#             if score > 95:    
#                 print(str(attr) + " is present in the file")
#                 print(ocr)
    else:
        print(str(attr) + " is NOT present in the file")
        
    print("===========================================")


# ### Checking distance between two address

# In[222]:


import geopy
from geopy.distance import geodesic,great_circle
from geopy.geocoders import Nominatim

def distance_between_address(addr1, addr2): 
    #definig locator
    locator = Nominatim(user_agent="https")
    
    # Getting location for each of the address
    loc1 = locator.geocode(addr1)
    loc2 = locator.geocode(addr2)
    
    # Extracting Latitude and Longitude
    add1_info = (loc1.latitude, loc1.longitude)
    add2_info = (loc2.latitude, loc2.longitude)
    
    return geodesic(add1_info, add2_info).miles    


# ### Checking attributes of address

# In[ ]:


import usaddress
def check_AddressNo(addr1, addr2):
    addr1_info, _ =  usaddress.tag(addr1)
    addr2_info, _ =  usaddress.tag(addr2)
    
    try:
        if addr1_info['AddressNumber'] ==  addr2_info['AddressNumber'] :
            return 1
        else:
            return -1
    except:
        return 0


# In[223]:


distance_between_address("1557 E. Florence Ave LA, CA 90001",
                         '1557 East Florence Ave Los Angeles, CA 90001')


# In[221]:


check_AddressNo("1557 E. Florence Ave, LA, CA 90001",
                '1557 East Florence Ave Los Angeles, CA 90001')


# In[ ]:
# In[225]:





# In[257]:


import phonenumbers
for n in ["htmls/www.healthcare4ppl.com.html", "htmls/www.doximity.com.html" , "htmls/www.dignityhealth.org.html", "htmls/doctor.webmd.com.html"]:
    file =  open(n, "r").read()

    h = html2text.HTML2Text()

    h.ignore_links = True
    h.skip_internal_links = True
    h.ignore_images = True

    read_file = h.handle(file)
    print("\n" + n)
    for match in phonenumbers.PhoneNumberMatcher(read_file, "US"):
        print(phonenumbers.format_number(match.number, phonenumbers.PhoneNumberFormat.E164))


# In[258]:


phonenumbers.format_number(match.number, phonenumbers.PhoneNumberFormat.E164)


# In[ ]:




